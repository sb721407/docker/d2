FROM nginx:1.17-alpine
COPY index.html /usr/share/nginx/html/index.html
COPY balancer.conf /etc/nginx/nginx.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]